## 在JavaScript中需要通过document.querySelector("#demo")来获取dom节点，然后再获取这个节点的值。在Vue中，我们不用获取dom节点，元素绑定ref之后，直接通过this.$refs即可调用，这样可以减少获取dom节点的消耗。

ref介绍
ref被用来给元素或子组件注册引用信息。引用信息将会注册在父组件的 $refs对象上。如果在普通的 DOM 元素上使用，引用指向的就是 DOM 元素；如果用在子组件上，引用就指向该子组件实例


ref 有三种用法：
1、ref 加在普通的元素上，用this.￥refs.（ref值） 获取到的是dom元素
2、 ref 加在子组件上 ， 用this.￥refs.(ref值) 获取到的就是组件实例 ，可以用组件的所有方法。 在使用方法的时候直接用 this.￥refs.(ref值).方法() 就能直接用了
3、如何利用 v-for 和 ref 获取一组数组或者dom 节点
注意点：
如果通过v-for 遍历想加不同的ref时记得加 :号，即 :ref =某变量 ;
这点和其他属性一样，如果是固定值就不需要加 :号，如果是变量记得加 :号。（加冒号的，说明后面的是一个变量或者表达式；没加冒号的后面就是对应的字符串常量（String））

## this.$refs介绍

this.$refs是一个对象，持有当前组件中注册过 ref特性的所有 DOM 元素和子组件实例

应注意的坑：
1、如果通过v-for 遍历想加不同的ref时记得加 :号，即 :ref =某变量 ;
这点和其他属性一样，如果是固定值就不需要加 :号，如果是变量记得加 :号。（加冒号的，说明后面的是一个变量或者表达式；没加冒号的后面就是对应的字符串常量（String））

2、通过 :ref =某变量 添加ref（即加了:号） ,如果想获取该ref时需要加 [0]，如this.r e f s [ r e f s A r r a y I t e m ] [ 0 ] ； 如 果 不 是 : r e f = 某 变 量 的 方 式 而 是 r e f = 某 字 符 串 时 则 不 需 要 加 ， 如 t h i s . refs[refsArrayItem] [0]；如果不是:ref =某变量的方式而是 ref =某字符串时则不需要加，如this.refs[refsArrayItem][0]；如果不是:ref=某变量的方式而是ref=某字符串时则不需要加，如this.refs[refsArrayItem]。

<template>
  <div @click="parentMethod">
    <children ref="children"></children>
  </div>
</template>
 
<script>
import children from 'components/children.vue'
export default {
  components: { 
    children 
  },
  data() {
    return {}
  },
  methods: {
    parentMethod() {
      this.$refs.children  //返回一个对象
      this.$refs.children.changeMsg() // 调用children的changeMsg方法
    }
  }
}
</script>
 
<style lang="sass" scoped></style>
