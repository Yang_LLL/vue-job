vue提供了v-on：事件绑定指令，用来辅助程序员为DOM元素绑定事件监听。

一·绑定事件

v-on：click="定义在vue中的方法"
v-on：input="定义在vue中的方法"
v-on：keyup="定义在vue中的方法"
v-on的简写为'@'.
 
 定义在vue中的方法在于data同层级methods里
            methods:{
            add: function(){}
            }
            //简写
            methods:{
            add(){}
            }

二·$sevent

如果在绑定事件的时候没有传参，则绑定的方法默认有一个参数（事件）对象e。如果在绑定事件时传参，则没有e。

vue提供了内置变量，名字叫$event,它是原生DOM的事件对象。在传参时把$event也传进去，就会有e。

2.1 事件修饰符

在事件处理函数中调用event.preventDefault()或event.stopPropageation(）是非常常见的需求。因此vue提供了事件修饰符的概念，来辅助程序员更方便的对事件的触发进行控制。例如：@click.prevent = "方法"

常用的2个事件修饰符如下：

事件修饰符	说明 
.prevent	阻止默认行为（例如：阻止a链接的跳转，阻止表单的提交等）
.stop	阻止事件冒泡