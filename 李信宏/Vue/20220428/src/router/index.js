// 配置路由规则
import VueRouter from 'vue-router'
//默认商品列表
import BodyBase from '../pages/BodyBase'
//默认商品广告列表
import AdvList from '../pages/AdvList'
import Detail from '../pages/Detail'
import CartList from '../pages/CartList'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/',
            component: BodyBase,
        },
        {
            path: '/base',
            component: BodyBase,
        },
        {
            path: '/adv',
            component: AdvList,
        },
        {
            name:'detail',
            //path:'/detail', props 传递参数，query 不行
            path: '/detail/:id/:name',
            component: Detail,
            //props:{id:'id1',name:'name1'}, 对象写法
            props:true //bool 类型
            // props($route){// 函数的写法
            //     return {
            //              id:$route.params.id,
            //              name:$route.params.name,
            //              img:$route.params.img
            //             }
            //    }
   
        },
        {
            name:'cartList',
            //path:'/detail',
            path: '/cartList',
            component: CartList,
            //props:{id:'id1',name:'name1'}, 对象写法
            props:true
        },
      
    ]
})

// export default router