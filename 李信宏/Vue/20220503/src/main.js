import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import Vuex from 'vuex';


//引入VueRouter
import VueRouter from 'vue-router'
//应用插件
Vue.use(VueRouter)

//应用vuex 
Vue.use(Vuex);

//vuex 是个 数据管理工具，配置
const store = new Vuex.Store({
  //状态,就是数据
  state: {
     title:'外卖系统',
     price:100,
  },

  mutations:{
     changeTile:function(state,payload){//state
         state.title = payload;
     }
  }
})


//引入路由器
import router from './router/index';

import plugin1 from './plugins/plugin1'

Vue.use(plugin1)




// axios.defaults.baseURL='http://123.207.32.32:8000';
Vue.prototype.$axios=axios;





//关闭生产环境提交
Vue.config.productionTip = false
//render 渲染 temldate.
new Vue({
  el:'#app',
  // template:'<App></App>',
  // components:{
  //   App,
  // }
  //脚手架引入的vue是残缺的，为了性能
  render: (createElement) => {
    // console.log(typeof createElement);
    return createElement(App)
   },
   beforeCreate:function(){
     Vue.prototype.$bus=this;
   },
   router,
   store
})
