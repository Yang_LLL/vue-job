import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import Vuex from 'vuex'
import  createVuexAlong from 'vuex-along'
import VueScroller from 'vue-scroller'
import axios from 'axios'


// clearVuexAlone()
Vue.prototype.$axios=axios;
Vue.use(VueScroller)
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
   nums:'1',
   cart:[
     {id:1,name:'',num:12,img:''},
     {id:2,name:'',num:12,img:''},
   ],
  },
  mutations:{
  changeNum(state){
    state.nums++
  },
  },
  getters:{
    getBuyNums(state){
      return state.nums
    }
  },
  plugins:[createVuexAlong()]
})


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
