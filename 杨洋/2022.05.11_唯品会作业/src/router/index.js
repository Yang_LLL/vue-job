import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            // name:'recommend',
            path: '/rem',
            component: () => import('../components/Recommend'),
            // props:true
        },
        {
            path: '/snapup',
            component: () => import('../components/SnapUp')
        },{
            path:'/shopping',
            component:()=>import('../components/Shooping')
        },
        {
            path:'/int',
            component:()=>import('../components/International')
        },{
            path:'/rel/:id/:name/:img/:price/:num',
            name:'RecommendList',
            component:()=>import('../pages/RecommendList'),
            props:true
        },
        {
            path:'/sul/:id/:name/:img/:price/:discount/:text/:money',
            name:'SnapUplist',
            component:()=>import('../pages/SnapUplist'),
            props:true
        },
        {
            path:'/shl/:id/:name/:img/:price/:num',
            name:"ShoopingList",
            component:()=>import('../pages/ShoopingList'),
            props:true
        },{
            path:'/intl/:id/:name/:img/:price/:money/:num',
            name:'InternationalList',
            component:()=>import("../pages/InternationalList"),
            props:true
        },{
            path:'/remcar',
            name:'remcar',
            meta:{
                keepAlive:true
            },
            component:()=>import('../delail/RemCar'),
            // props:true
        },{
            path:'/shoopingcart',
            name:'ShoopingCart',
            component:()=>import('../delail/ShoopingCart'),
            // props:true
        }
    ]
})