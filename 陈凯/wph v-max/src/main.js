import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router"
import router from "./router/Router"
import VueScrol from "vue-scroller"

Vue.use(VueRouter)
Vue.use(VueScrol)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
