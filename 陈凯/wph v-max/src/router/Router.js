import VueRouter from "vue-router"
import TuiJian from "../components/tuijian"
import Qiang from "../components/qiang"

export default new VueRouter({
    routes: [
        {
            path: "/",
            component: () => import("../components/top"),
        },
        {
            path: "/nav",
            component: () => import("../components/top"),
            children:[
                {
                    path: "tuijian",
                    component: TuiJian,
                },
                {
                    path: "qiang",
                    component: Qiang,
                },
            ]
        },
        {
            path: "/shop",
            component: () => import("../components/shop"),
        },
        {
            path:"/car",
            component:() => import("../components/car")
        },
        {
            path:"/loding",
            component:() => import("../components/loding")
        }
    ]
})

