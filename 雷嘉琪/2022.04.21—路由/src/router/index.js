import Vue from "vue";
import VueRouter from "vue-router";
import NvZhuang from "../pages/NvZhuang.vue";
import CaiHu from "../pages/CaiHu.vue";
import ShouShi from "../pages/ShouShi.vue";
import TongZhuang from "../pages/TongZhuang.vue";
// import Detail from "../pages/Detail.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/NvZhuang",
    component: NvZhuang,
  },
  {
    path: "/CaiHu",
    component: CaiHu
  },
  {
    path: "/ShouShi",
    component: ShouShi,
  },
  {
    path: "/TongZhuang",
    component: TongZhuang
  },
  // {
  //   path: "/detail",
  //   component: Detail
  // },

];

const router = new VueRouter({
  routes,
});

export default router;