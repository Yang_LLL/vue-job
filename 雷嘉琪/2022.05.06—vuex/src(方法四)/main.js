import Vue from 'vue';
import App from './App.vue';

import VueRouter from 'vue-router';
import Vuex from 'vuex';

import router from './router/index';
Vue.config.productionTip = false;

// 应用插件
Vue.use(VueRouter);
Vue.use(Vuex);

// vuex 配置
const store = new Vuex.Store({
    state: {
      title: '人工智能',
      price: 100000,
    }
})

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
