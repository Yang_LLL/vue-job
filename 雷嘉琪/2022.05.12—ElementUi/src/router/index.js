import Vue from "vue";
import VueRouter from "vue-router";

import NvZhuang from "../pages/NvZhuang.vue";
import CaiHu from "../pages/CaiHu.vue";
import ShouShi from "../pages/ShouShi.vue";
import TongZhuang from "../pages/TongZhuang.vue";

import Ele from '../pages/Ele.vue';

import Particulars from "../pages/Particulars.vue";
import TrolleyList from "../pages/TrolleyList.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/NvZhuang",
    component: NvZhuang,
  },
  {
    path: "/CaiHu",
    component: CaiHu
  },
  {
    path: "/ShouShi",
    component: ShouShi,
  },
  {
    path: "/TongZhuang",
    component: TongZhuang
  },
  {
    path: "/Ele",
    component: Ele
  },
  {
    name: 'Particulars',
    path: "/Particulars/:id/:name/:price/:img/:num",
    component: Particulars,
    props: true
  },
  {
    name: 'TrolleyList',
    path: "/TrolleyList",
    component: TrolleyList,
    props: true //bool 类型
  },

];

const router = new VueRouter({
  routes,
});

export default router;