var express = require('express');
var router = require('./router/router.js')
//2、创建web服务器  app接收
var app = express();
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", '3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();//执行下一个中间件
});
//路由已经放到routes.js中
app.use(router);//使用
//4、启动服务listen(端口号,回调函数)
app.listen(9090, () => {
    console.log('服务器开启 http://127.0.0.1:9090');
})