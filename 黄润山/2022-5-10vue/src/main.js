import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import Vuex from 'vuex';

import VueScroller from 'vue-scroller'
Vue.use(VueScroller)

//引入VueRouter
import VueRouter from 'vue-router'
//应用插件
Vue.use(VueRouter)

Vue.use(Vuex);



const store = new Vuex.Store({
   state: {  
        name:'外卖系统',
        cart:[],
        buyNums:0,//购买的数量，每次点击购买时，把对应购买数量+1

    },    

    mutations:{
      changeName(state,name){
          state.name=name;
        
      },
      changeBuyNum(state,num){
          console.log('change buynum');
          //请求后台数据,
          axios.get('https://question.llblog.cc/api.php').then(res=>{
            console.log(res);
            state.buyNums = state.buyNums+num; //0+1    1+1    0
            console.log(state.buyNums);
          });
        
      },
      addToCart(state,product){
        let inCart = false;
         state.cart.forEach(el=>{
            if(el.id==product.id){
               el.num++
               inCart=true;
            }
         })
         if(!inCart){
           state.push(product);
         }
      },
     
    
    },
    actions:{
      changeNumByAction (context,num) {
        console.log(context);//context 是store的精简版
        context.commit('changeBuyNum',num)
      }
    },
    getters:{
      getBuyNums(state){
         return state.buyNums+' ====数量';
      }
    }

 })

//引入路由器
import router from './router/index';



// axios.defaults.baseURL='http://123.207.32.32:8000';

Vue.prototype.$axios=axios;

//关闭生产环境提交
Vue.config.productionTip = false
//render 渲染 temldate.
new Vue({
  el:'#app',
  // template:'<App></App>',
  // components:{
  //   App,
  // }
  //脚手架引入的vue是残缺的，为了性能
  render: (createElement) => {
    // console.log(typeof createElement);
    return createElement(App)
   },
   beforeCreate:function(){
     Vue.prototype.$bus=this;
   },
   router,
   store
})
