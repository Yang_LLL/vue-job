// 配置路由规则
import VueRouter from 'vue-router'
//默认商品列表
import BodyBase from '../pages/BodyBase'
//默认商品广告列表
import AdvList from '../pages/AdvList'
import Detail from '../pages/Detail'
import CartList from '../pages/CartList'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/',
            component: BodyBase,
        },
        {
            path: '/base',
            component: BodyBase,
        },
        {
            name:'listName',
            path: '/adv',
            component: AdvList,
            meta:{
                keepalive:true
            }
        },
        {
            name:'detail',
            //path:'/detail',
            path: '/detail/:id/:name',
            component: Detail,
            //props:{id:'id1',name:'name1'}, 对象写法
            props:true
        },
        {
            name:'cartList',
            //path:'/detail',
            path: '/cartList',
            component: CartList,
            //props:{id:'id1',name:'name1'}, 对象写法
            props:true
        },
      
    ]
})

// export default router